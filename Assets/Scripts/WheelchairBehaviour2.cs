﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WheelchairBehaviour2 : MonoBehaviour
{
    public List<AxleInfo> axleInfos; // the information about each individual axle
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    public float maxBrakeTorque; // maximum brake torque can be applied to wheel
    public float steeringMotorTorque; // torque the motor applies to wheel while steering

    public string LastCommand { private set; get; }

    private bool forwardDirection = true;


    // Start is called before the first frame update
    void Start()
    {
        DefineBaseSteeringAngles();
    }

    private void DefineBaseSteeringAngles() {
        foreach (AxleInfo axleInfo in axleInfos) {
            Quaternion rotation;
            Vector3 position;

            axleInfo.rightWheel.GetWorldPose(out position, out rotation);
            axleInfo.prevPositionRight = position;

            axleInfo.leftWheel.GetWorldPose(out position, out rotation);
            axleInfo.prevPositionLeft = position; 
        }
    }

    // finds the corresponding visual wheel
    // correctly applies the transform
    private void ApplyLocalPositionToVisuals(WheelCollider collider) {
        if (collider.transform.childCount == 0) {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    private void WHTurn(AxleInfo axleInfo, bool forward, bool right) {
        if (axleInfo.motor) {
            float steeringMotor = steeringMotorTorque;
            if (!forwardDirection) {
                steeringMotor *= -1; // if WH moves back motor direction changes to opposite
            }

            axleInfo.leftWheel.motorTorque = steeringMotor;
            axleInfo.rightWheel.motorTorque = steeringMotor;

            if (forward && right) {
                axleInfo.leftWheel.brakeTorque = 0;
                axleInfo.rightWheel.brakeTorque = maxBrakeTorque;
            }
            if (forward && !right) {
                axleInfo.leftWheel.brakeTorque = maxBrakeTorque;
                axleInfo.rightWheel.brakeTorque = 0;
            }
            if (!forward && right) {
                axleInfo.leftWheel.brakeTorque = maxBrakeTorque;
                axleInfo.rightWheel.brakeTorque = 0;
            }
            if (!forward && !right) {
                axleInfo.leftWheel.brakeTorque = 0;
                axleInfo.rightWheel.brakeTorque = maxBrakeTorque;
            }
        }
    }

    private void WHMove(AxleInfo axleInfo, bool forward) {
        if (axleInfo.motor) {
            float motor = maxMotorTorque;

            if (forward) {
                forwardDirection = true;
            }
            else {
                motor *= -1; // if WH moves back motor direction changes to opposite
                forwardDirection = false;
            }

            axleInfo.leftWheel.brakeTorque = 0;
            axleInfo.rightWheel.brakeTorque = 0;
            axleInfo.leftWheel.motorTorque = motor;
            axleInfo.rightWheel.motorTorque = motor;
        }
    }

    private void WHStop(AxleInfo axleInfo) {
        if (axleInfo.motor) {
            axleInfo.leftWheel.brakeTorque = maxBrakeTorque;
            axleInfo.rightWheel.brakeTorque = maxBrakeTorque;
        }
    }

    public void act(String command) {
        // Debug.Log("got command "+command);
        LastCommand = command;

        foreach (AxleInfo axleInfo in axleInfos) {

            switch (command)
            {
                case "STOP":
                WHStop(axleInfo);
                break;
                case "GO":
                WHMove(axleInfo, true);
                break;
                case "BACK":
                WHMove(axleInfo, false);
                break;
                case "LEFT":
                WHTurn(axleInfo, forwardDirection, false);
                break;
                case "RIGHT":
                WHTurn(axleInfo, forwardDirection, true);
                break;
                case "update":
                break;
                default:
                Debug.Log("Unknown command");
                break;
            }

            if (axleInfo.steering) {
                steerWheels(axleInfo);
            }

            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }
    }

    private void steerWheels(AxleInfo axleInfo) {
        Quaternion rotation;
        Vector3 position;

        axleInfo.leftWheel.GetWorldPose(out position, out rotation);
        axleInfo.leftWheel.steerAngle = Vector3.Angle(
            position,
            axleInfo.rightWheel.transform.forward);
        axleInfo.prevPositionLeft = position;
        Debug.Log("position = "+position);
        Debug.Log("axleInfo.prevPositionLeft = "+axleInfo.prevPositionLeft);
        Debug.Log("axleInfo.leftWheel.transform.forward = "+axleInfo.leftWheel.transform.forward);
        Debug.Log("axleInfo.leftWheel.steerAngle = "+axleInfo.leftWheel.steerAngle);

        axleInfo.rightWheel.GetWorldPose(out position, out rotation);
        axleInfo.rightWheel.steerAngle = Vector3.Angle(
            position-axleInfo.prevPositionRight,
            axleInfo.rightWheel.transform.forward);
        axleInfo.prevPositionRight = position;
        // Debug.Log("axleInfo.rightWheel.steerAngle = "+axleInfo.rightWheel.steerAngle);
    }
}

[System.Serializable]
public class AxleInfo {
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor; // is this wheel attached to motor?
    public bool steering; // does this wheel apply steer angle?

    public Vector3 prevPositionLeft { set; get; }
    public Vector3 prevPositionRight { set; get; }
}