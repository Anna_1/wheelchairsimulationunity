﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System;
using UnityEngine.UI;
using UnityEngine;



public class ClientScript : MonoBehaviour
{
    public bool debug = false;

    public WheelchairBehaviour3 wheelchair;
    // public List<Lidar> lidars;
    public Lidar2 frontLidar;
    public Lidar2 backLidar;
    public Text textLabel;

    private Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    private IPAddress ip = IPAddress.Parse("127.0.0.1");
    private int port = 9990;

    // Flag to let us know a connection has dropped.
    private bool dropped = false;

    //required for stream parsing where client may recv multiple messages at once.
    const string packetTerminationChar = "\n";

    byte[] ask_msg = { 49 };
    byte[] bytes;

    // Start is called before the first frame update
    void Start()
    {
        // Socket
        try
        {
            client.Connect(ip, port);
            Debug.Log("connection");
        }
        catch (SocketException ex)
        {
            Debug.Log("no connection: " + ex.Message);
        }

        dropped = false;

        InitialSend();
    }

    void InitialSend()
    {
        if(client != null && !dropped)
        {
            if(!client.Connected)
            {
                dropped = true;
                return;
            }

            JSONObject jsonMsg = getAllParamsAsJSON();

            try
            {
                SendJSONData(jsonMsg);
            }
            catch(SocketException ex)
            {
                Debug.LogWarning("connection dropped: " + ex.Message);
                dropped = true;
            }
        }
    }

    // Update is called once per frame
    // FixedUpdate is called once per 0.02s (Edit -> Project Settings -> Time)
    void FixedUpdate()
    {
        if(client != null && !dropped)
        {
            if(!client.Connected)
            {
                dropped = true;
                return;
            }

            String command = "";
            JSONObject jsonMsg = getAllParamsAsJSON();

            try
            {
                command = ReceiveCommand();
            }
            catch(SocketException ex)
            {
                Debug.LogWarning("connection dropped: " + ex.Message);
                dropped = true;
            }
            
            ProcessCommand(command);

            try
            {
                if (command != "") SendJSONData(jsonMsg);
            }
            catch(SocketException ex)
            {
                Debug.LogWarning("connection dropped: " + ex.Message);
                dropped = true;
            }
        }
    }

    private void ProcessCommand(String command)
    {
        string[] com_params = command.Split(new char[] { ';' });
        if (com_params.Length < 6)
        {
            if (debug) Debug.Log("update " + command);
            wheelchair.actUpdate();
        }
        else
        {
            try
            {
                wheelchair.act(float.Parse(com_params[0], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[1], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[2], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[3], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[4], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[5], CultureInfo.InvariantCulture.NumberFormat));
            }
            catch
            {
                Debug.Log("Can't parse command: " + command);
            }

            if (com_params.Length > 6)
            {
                textLabel.text = com_params[6];
                // Debug.Log("command " + com_params[6]);
                Debug.Log("command " + command);
            }
        }
    }

    // private void ProcessCommand(String command)
    // {
    //     if (command != "")
    //     {
    //         wheelchair.act(command);
    //         textLabel.text = command;
    //     }
    //     else
    //     {
    //         wheelchair.act("update");
    //     }
    // }

    private String ReceiveCommand()
    {
        String command = "";

        if (client.Available > 0)
        {
            bytes = new byte[client.Available];
            client.Receive(bytes);
            command = System.Text.UTF8Encoding.UTF8.GetString(bytes);
            // wheelchair.act(command);
        }

        return command;
    }

    private JSONObject getAllParamsAsJSON()
    {
        JSONObject json = JSONObject.Create();
        json = getLidarsDataAsJson(json);
        json = getWheelchairConfigsAsJson(json);

        return json;
    }

    private JSONObject getWheelchairConfigsAsJson(JSONObject json)
    {
        json.AddField("maxMotorTorque", Lidar2.toDecimalBakedJSONObject(wheelchair.maxMotorTorque));
        json.AddField("maxBrakeTorque", Lidar2.toDecimalBakedJSONObject(wheelchair.maxBrakeTorque));
        json.AddField("steeringMotorTorque", Lidar2.toDecimalBakedJSONObject(wheelchair.steeringMotorTorque));
        json.AddField("baseAdjacentSteeringAngle", Lidar2.toDecimalBakedJSONObject(wheelchair.baseAdjacentSteeringAngle));
        json.AddField("baseOppositeSteeringAngle", Lidar2.toDecimalBakedJSONObject(wheelchair.baseOppositeSteeringAngle));

        return json;
    }

    private JSONObject getLidarsDataAsJson(JSONObject json)
    {
        if (frontLidar)
        {
            JSONObject front = frontLidar.GetOutputAsJson();
            if (front.Count > 0) json.AddField("frontLidar", front);
        }

        if (backLidar)
        {
            JSONObject back = backLidar.GetOutputAsJson();
            if (back.Count > 0) json.AddField("backLidar", back);
        }

        return json;
    }

    private bool SendData(byte[] data)
    {
        if (!client.Connected)
        return false;

        client.Send(data);

        if (debug)
        {
            Debug.Log("sent: " + System.Text.Encoding.Default.GetString(data));
        }

        return true;
    }

    private bool SendDataAsync(byte[] data)
    {
        if (!client.Connected) return false;

        SocketAsyncEventArgs socketAsyncData = new SocketAsyncEventArgs();
        socketAsyncData.SetBuffer(data, 0, data.Length);
        client.SendAsync(socketAsyncData);

        if (debug)
        {
            Debug.Log("sent: " + System.Text.Encoding.Default.GetString(data));
        }

        return true;
    }

    // Send a json packet over TCP socket
    private void SendJSONData(JSONObject msg, bool async=true)
    {
        string msg_str = msg.ToString();
        if (msg_str == "null")
        msg_str = "{}";

        string packet = msg_str + packetTerminationChar;;

        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(packet);

        if (async)
        {
            SendDataAsync(bytes);
        }
        else
        {
            SendData(bytes);
        }
    }

    public void Disconnect()
    {
        try
        {
            if (client.Connected)
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes("0"+packetTerminationChar);
                SendData(bytes);

                client.Shutdown(SocketShutdown.Both);

                if (!dropped)
                {
                    client.Disconnect(true);
                }
            }
        }
        catch(SocketException e)
        {
            Debug.Log(e.ToString());
        }
        finally
        {
            client.Close();
        }
    }

    void OnDestroy()
    {
        Disconnect();
    }
}
