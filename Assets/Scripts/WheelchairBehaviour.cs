using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WheelchairBehaviour : MonoBehaviour
{
    public List<AxleInfo> axleInfos; // the information about each individual axle
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    public float maxBrakeTorque; // maximum brake torque can be applied to wheel
    public float steeringMotorTorque; // torque the motor applies to wheel while steering

    public string LastCommand { private set; get; }

    private float baseAdjacentSteeringAngle;
    private float baseOppositeSteeringAngle;

    private float steeringLeft = 0.0f;
    private float steeringRight = 0.0f;
    private int steerDirectionForLeftWheel = 1;
    private int steerDirectionForRightWheel = 1;
    private bool forwardDirection = true;

    // Start is called before the first frame update
    void Start()
    {
        DefineBaseSteeringAngles();
    }

    private void DefineBaseSteeringAngles() {
        Quaternion rotation;
        Vector3 position0;
        Vector3 positionAdjacent;
        Vector3 positionOpposite;
        position0.x = 0;
        position0.z = 0;
        positionAdjacent.x = 0;
        positionAdjacent.z = 0;
        positionOpposite.x = 0;
        positionOpposite.z = 0;
        foreach (AxleInfo axleInfo in axleInfos) {
            if (axleInfo.motor) {
                axleInfo.rightWheel.GetWorldPose(out position0, out rotation);
            }
            if (axleInfo.steering) {
                axleInfo.rightWheel.GetWorldPose(out positionAdjacent, out rotation);
                axleInfo.leftWheel.GetWorldPose(out positionOpposite, out rotation);
            }
        }

        baseAdjacentSteeringAngle = DefineAngle(position0.x, position0.z, positionAdjacent.x, positionAdjacent.z);
        baseOppositeSteeringAngle = DefineAngle(position0.x, position0.z, positionOpposite.x, positionOpposite.z);
    }

    private float DefineAngle(float x0, float z0, float x1, float z1) {

        double adjacentLeg = Math.Abs(x1 - x0);
        if (adjacentLeg == 0) {
            return 90.0f;
        }

        double oppositeLeg = Math.Abs(z1 - z0);
        double radianAngle = Math.Atan(oppositeLeg/adjacentLeg);

        return (float) RadianToDegree(radianAngle);
    }

    private double RadianToDegree(double angle) { return angle * (180.0 / Math.PI); }

    // finds the corresponding visual wheel
    // correctly applies the transform
    private void ApplyLocalPositionToVisuals(WheelCollider collider) {
        if (collider.transform.childCount == 0) {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    private void WHTurn(AxleInfo axleInfo, bool forward, bool right) {
        if (axleInfo.motor) {
            float steeringMotor = steeringMotorTorque;
            if (!forwardDirection) {
                steeringMotor *= -1; // if WH moves back motor direction changes to opposite
            }

            axleInfo.leftWheel.motorTorque = steeringMotor;
            axleInfo.rightWheel.motorTorque = steeringMotor;

            if (forward && right) {
                axleInfo.leftWheel.brakeTorque = 0;
                axleInfo.rightWheel.brakeTorque = maxBrakeTorque;

                steeringLeft = baseOppositeSteeringAngle;
                steeringRight = baseAdjacentSteeringAngle;
            }
            if (forward && !right) {
                axleInfo.leftWheel.brakeTorque = maxBrakeTorque;
                axleInfo.rightWheel.brakeTorque = 0;

                steeringLeft = 360.0f - baseAdjacentSteeringAngle;
                steeringRight = 360.0f - baseOppositeSteeringAngle;
            }
            if (!forward && right) {
                axleInfo.leftWheel.brakeTorque = maxBrakeTorque;
                axleInfo.rightWheel.brakeTorque = 0;

                steeringLeft = 180.0f - baseAdjacentSteeringAngle;
                steeringRight = 180.0f - baseOppositeSteeringAngle;
            }
            if (!forward && !right) {
                axleInfo.leftWheel.brakeTorque = 0;
                axleInfo.rightWheel.brakeTorque = maxBrakeTorque;

                steeringLeft = 180.0f + baseOppositeSteeringAngle;
                steeringRight = 180.0f + baseAdjacentSteeringAngle;
            }
        }

        GetSteeringSettings(axleInfo);
    }

    private void GetSteeringSettings(AxleInfo axleInfo) {
        if (axleInfo.steering) {
            int delta = (int)steeringLeft - (int)axleInfo.leftWheel.steerAngle;
            if (delta != 0) {
                steerDirectionForLeftWheel = delta / Math.Abs(delta);
                if (Math.Abs(delta)>=180) { steerDirectionForLeftWheel *= -1; }
            }

            delta = (int)steeringRight - (int)axleInfo.rightWheel.steerAngle;
            if (delta != 0) {
                steerDirectionForRightWheel = delta / Math.Abs(delta);
                if (Math.Abs(delta)>180) { steerDirectionForRightWheel *= -1; }
            }
        }
    }

    private void WHMove(AxleInfo axleInfo, bool forward) {
        if (axleInfo.motor) {
            float motor = maxMotorTorque;

            if (forward) {
                forwardDirection = true;
                steeringLeft = 0.0f;
                steeringRight = 0.0f;
            }
            else {
                motor *= -1; // if WH moves back motor direction changes to opposite
                forwardDirection = false;
                steeringLeft = 180.0f;
                steeringRight = 180.0f;
            }


            axleInfo.leftWheel.brakeTorque = 0;
            axleInfo.rightWheel.brakeTorque = 0;
            axleInfo.leftWheel.motorTorque = motor;
            axleInfo.rightWheel.motorTorque = motor;
        }

        GetSteeringSettings(axleInfo);
    }

    private void WHStop(AxleInfo axleInfo) {
        if (axleInfo.motor) {
            axleInfo.leftWheel.brakeTorque = maxBrakeTorque;
            axleInfo.rightWheel.brakeTorque = maxBrakeTorque;
        }
        if (axleInfo.steering) {
            // if WH stops wheels shouldn't continue steering
            steeringLeft = axleInfo.leftWheel.steerAngle;
            steeringRight = axleInfo.rightWheel.steerAngle;
        }
    }

    private void SteerToAngle(WheelCollider wheel, float steerAngle, int direction) {
        wheel.steerAngle %= 360;
        if ((int)wheel.steerAngle != (int)steerAngle) {
            if ((int)wheel.steerAngle <= 0 && direction < 0) {
                wheel.steerAngle += 360;
            }
            wheel.steerAngle += 1.0f*direction;
        }
    }

    public void act(String command) {
        // Debug.Log("got command "+command);
        LastCommand = command;

        foreach (AxleInfo axleInfo in axleInfos) {

            switch (command)
            {
                case "STOP":
                WHStop(axleInfo);
                break;
                case "GO":
                WHMove(axleInfo, true);
                break;
                case "BACK":
                WHMove(axleInfo, false);
                break;
                case "LEFT":
                WHTurn(axleInfo, forwardDirection, false);
                break;
                case "RIGHT":
                WHTurn(axleInfo, forwardDirection, true);
                break;
                case "update":
                break;
                default:
                Debug.Log("Unknown command");
                break;
            }

            if (axleInfo.steering) {
                SteerToAngle(axleInfo.leftWheel, steeringLeft, steerDirectionForLeftWheel);
                SteerToAngle(axleInfo.rightWheel, steeringRight, steerDirectionForRightWheel);
            }

            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }
    }
}

// [System.Serializable]
// public class AxleInfo {
//     public WheelCollider leftWheel;
//     public WheelCollider rightWheel;
//     public bool motor; // is this wheel attached to motor?
//     public bool steering; // does this wheel apply steer angle?
// }