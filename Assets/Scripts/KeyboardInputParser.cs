using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class KeyboardInputParser : MonoBehaviour
{
    public WheelchairBehaviour3 wheelchair;
    public Text textLabel;

    private bool _forward = true;

    public void FixedUpdate()
    {
        String command = "";

        // // Comment, if WheelchairBehaviour class is used
        // if (Input.GetKey(KeyCode.Space)) { command="STOP"; }
        // if (Input.GetKey(KeyCode.UpArrow)) { command="GO"; }
        // if (Input.GetKey(KeyCode.DownArrow)) { command="BACK"; }
        // if (Input.GetKey(KeyCode.LeftArrow)) { command="LEFT"; }
        // if (Input.GetKey(KeyCode.RightArrow)) { command="RIGHT"; }

        // if (!String.IsNullOrEmpty(command)) { wheelchair.act(command); }
        // else { wheelchair.act("update"); }

        // if (command != "") {
        //     textLabel.text = command;
        // }


        // Comment, if WheelchairBehaviour3 class is used
        if (Input.GetKey(KeyCode.Space)) { command="0.0;0.0;10.0;10.0;-1.0;-1.0;STOP"; }
        if (Input.GetKey(KeyCode.UpArrow)) { command="4.0;4.0;0.0;0.0;0.0;0.0;GO"; _forward = true; }
        if (Input.GetKey(KeyCode.DownArrow)) { command="-4.0;-4.0;0.0;0.0;180.0;180.0;BACK"; _forward = false; }
        if (Input.GetKey(KeyCode.LeftArrow) && _forward) { command="2.0;2.0;10.0;0;270.0;330.0;LEFT"; }
        if (Input.GetKey(KeyCode.LeftArrow) && !_forward) { command="-2.0;-2.0;0;10.0;210.0;270.0;LEFT"; }
        if (Input.GetKey(KeyCode.RightArrow) && _forward) { command="2.0;2.0;0;10.0;30.0;90.0;RIGHT"; }
        if (Input.GetKey(KeyCode.RightArrow) && !_forward) { command="-2.0;-2.0;10.0;0;90.0;150.0;RIGHT"; }

        ProcessCommand(command);

    }

    private void ProcessCommand(String command)
    {
        string[] com_params = command.Split(new char[] { ';' });
        if (com_params.Length < 6)
        {
            // Debug.Log("update " + command);
            wheelchair.actUpdate();
        }
        else
        {
            try
            {
                wheelchair.act(float.Parse(com_params[0], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[1], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[2], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[3], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[4], CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(com_params[5], CultureInfo.InvariantCulture.NumberFormat));
            }
            catch
            {
                Debug.Log("Can't parse command: " + command);
            }

            if (com_params.Length > 6)
            {
                textLabel.text = com_params[6];
                Debug.Log(command);
            }
        }
    }
}